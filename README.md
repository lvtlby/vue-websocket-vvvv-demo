# vue-websocket-demo

## Project setup (Main)
```
npm install
```

### Run WebSocket Server and HTTP dev (win)
```
runall.bat
```

### Run VVVV UDP Server Demo 
```
udp-server-demo.v4p
```

## Project setup 

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run WebSocket Server
```
npm run ws
```


## Customize configuration

### HTTP Server
```
vue.config.js
port: '8888'
```

### WebSocket Server
```
src/App.vue
line 38-> host: '192.168.0.109'
line 39-> port: '5555'
ws-server.js
line 9 -> port: 5555
```

### UDP Server
```
ws-server.js
line 5-> const udpServerHost = '192.168.0.109'
line 6-> const udpServerPort = 6666
vvvv file
udp server port 6666
```