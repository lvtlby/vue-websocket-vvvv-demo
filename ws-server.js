const WebSocket = require('ws');
const dgram = require('dgram');

const udpClient = dgram.createSocket('udp4');
const udpServerHost = '192.168.0.109';
const udpServerPort = 6666;


const wss = new WebSocket.Server({ port: 5555 });

wss.on('connection', (ws) => {

  ws.on('message', (message) => {
    console.log('received: %s', message);
    udpClient.send(message, udpServerPort, udpServerHost);
  });

  ws.on('open', () => {
    console.log('connected');
  });

  ws.on('closed', () => {
    console.log('disconnected');
  });

});
